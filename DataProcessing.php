<?php
include ('Connection.php');

class DataProcessing extends Connection
{
    private $name;
    private $unknownFaceObject;
    private $expressions;

    public function set($data = array()){
        if (array_key_exists('name', $data)) {
//            if ($data['name'] == 'unknown'){
//                $this->name = 'Person 1';
//            }
//            else{
//                $this->name = $data['name'];
//            }
            $this->name = $data['name'];
        }
        if (array_key_exists('express', $data)) {
            $this->expressions['expression'] = $data['express'];
        }
        if (array_key_exists('angry', $data)) {
            $this->expressions['angry'] = $data['angry'];
        }
        if (array_key_exists('disgusted', $data)) {
            $this->expressions['disgusted'] = $data['disgusted'];
        }
        if (array_key_exists('fearful', $data)) {
            $this->expressions['fearful'] = $data['fearful'];
        }
        if (array_key_exists('happy', $data)) {
            $this->expressions['happy'] = $data['happy'];
        }
        if (array_key_exists('neutral', $data)) {
            $this->expressions['neutral'] = $data['neutral'];
        }
        if (array_key_exists('sad', $data)) {
            $this->expressions['sad'] = $data['sad'];
        }
        if (array_key_exists('surprised', $data)) {
            $this->expressions['surprised'] = $data['surprised'];
        }
//        if (array_key_exists('unknownFaceObject', $data)) {
//            $this->unknownFaceObject['unknownFaceObject'] = $data['unknownFaceObject'];
//        }
////        var_dump($this);
//        return $this->postID;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `face` (`name`, `expression`, `angry`, `disgusted`, `fearful`, `happy`, `neutral`, `sad`, `surprised`) VALUES (:name, :expression, :angry, :disgusted, :fearful, :happy, :neutral, :sad, :surprised)");
            $result =  $stmt->execute(array(
                ':name' => $this->name,
                ':expression' => $this->expressions['expression'],
                ':angry' => $this->expressions['angry'],
                ':disgusted' => $this->expressions['disgusted'],
                ':fearful' => $this->expressions['fearful'],
                ':happy' => $this->expressions['happy'],
                ':neutral' => $this->expressions['neutral'],
                ':sad' => $this->expressions['sad'],
                ':surprised' => $this->expressions['surprised']
            ));

//            var_dump('this->postID');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
//                header('postDetails:live.php');
//                echo $this->postID;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `face`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function totalStudents($startID, $endID){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(DISTINCT `name`) AS `totalStudents` FROM `face` WHERE `id` >= '$startID' AND `id` <= '$endID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function studentsExpression($startID, $endID){
        try{
            $stmt = $this->con->prepare("SELECT `name`, `expression` FROM `face` WHERE `id` >= '$startID' AND `id` <= '$endID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function studentsName($startID, $endID){
        try{
            $stmt = $this->con->prepare("SELECT DISTINCT `name` FROM `face` WHERE `id` >= '$startID' AND `id` <= '$endID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

}
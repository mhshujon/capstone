<?php include_once ('header.php');
include ('DataProcessing.php');
session_start();
session_destroy();

$object = new DataProcessing();
$data = $object->index();
//echo $data[0]['datetime'];
//var_dump($data);
?>
    <!--==========================
      Header
      ============================-->
    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left-up">
                <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
                <!-- Uncomment below if you prefer to use a text logo -->
                <!--<h1><a href="#hero">Regna</a></h1>-->
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="index.php">Home</a></li>
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#contact">Contact Us</a></li>
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->


    <!--==========================
     Hero Section
   ============================-->
    <section id="hero">
        <div class="hero-container">
            <h1>Result Requirements</h1>
            <form class="modal-content" style="background: transparent; border: transparent" method="POST" action="totalstatus.php">
                <div class="md-form col-md-6">
                    <select required="" class="form-control form-control-sm btn-get-started" name="startDateTime" style="color: black; margin-left: 340px; max-width: 240px">
                        <option value="" disabled="" selected="">Start Date & Time</option>
                        <?php for ($i=0; $i<sizeof($data); $i++):?>
                            <!--                        --><?php //$_GET['id']= $data[$i]['id']?>
                            <option value="<?php echo $data[$i]['id']?>"><?php echo $i+1 .') '.$data[$i]['datetime']?></option>
                        <?php endfor;?>
                    </select>
                    <select required="" class="form-control form-control-sm btn-get-started" name="endDateTime" style="color: black; margin-left: 340px; max-width: 240px">
                        <option value="" disabled="" selected="">End Date & Time</option>
                        <?php for ($i=0; $i<sizeof($data); $i++):?>
                            <option value="<?php echo $data[$i]['id']?>"><?php echo $i+1 .') '.$data[$i]['datetime']?></option>
                        <?php endfor;?>
                    </select>
                    <div style="margin-left: 400px">
                        <div class="btn-get-started">
                            <button type="submit" style="background: transparent; border: transparent">Show Result</button>
                        </div>
                        <a href="live_video.php" class="btn-get-started">Test Again</a>
                    </div>
                </div>
            </form>
        </div>

    </section><!-- #hero -->

<?php include_once ('footer.php')?>
<?php
session_start();
$_SESSION['testCondition'] = 'imgUpload';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Live video</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left-up">
        <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
        <!-- Uncomment below if you prefer to use a text logo -->
        <!--<h1><a href="#hero">Regna</a></h1>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="index.php">Home</a></li>
          <li><a href="#about">About Us</a></li>
<!--          <li class="menu-has-children"><a href="">Drop Down</a>-->
<!--            <ul>-->
<!--              <li><a href="#">Drop Down 1</a></li>-->
<!--              <li class="menu-has-children"><a href="#">Drop Down 2</a>-->
<!--                <ul>-->
<!--                  <li><a href="#">Deep Drop Down 1</a></li>-->
<!--                  <li><a href="#">Deep Drop Down 2</a></li>-->
<!--                  <li><a href="#">Deep Drop Down 3</a></li>-->
<!--                  <li><a href="#">Deep Drop Down 4</a></li>-->
<!--                  <li><a href="#">Deep Drop Down 5</a></li>-->
<!--                </ul>-->
<!--              </li>-->
<!--              <li><a href="#">Drop Down 3</a></li>-->
<!--              <li><a href="#">Drop Down 4</a></li>-->
<!--              <li><a href="#">Drop Down 5</a></li>-->
<!--            </ul>-->
<!--          </li>-->
          <li><a href="#contact">Contact Us</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->


   <!--==========================
    Hero Section
  ============================-->
  <section id="hero">
    <div class="hero-container">
      <h1>Upload Picture</h1>
     
     <!-- <a href="" class="btn-get-started">Upload</a>-->
      

      <div class="pull-center">
        <div class="input-group">
            <span class="btn-get-started">
                <span class="btn btn-default btn-file">
                    <input type="File" id="imageUpload">
                </span>
            </span>
           
        </div>
        <img id='img-upload'/>
       </div>
    </div>


  </section><!-- #hero -->
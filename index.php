<?php include_once ('header.php');
//$_POST['name'] = ['Monir (5.0)', 'Samlan (2.0)'];
//$_POST['age'] = [20, 22];
//$_POST['location'] = [2012, 2222];
////var_dump($_POST['name'][0]);
////echo sizeof($_POST['name'])
//for ($i=0; $i<sizeof($_POST['name']); $i++){
//    $temp = explode(' ', $_POST['name'][$i]);
//    $_POST['name'][$i] = $temp[0];
//}
//var_dump($_POST);
?>

<!--==========================
  Header
  ============================-->
<header id="header">
    <div class="container">

        <div id="logo" class="pull-left-up">
            <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
            <!-- Uncomment below if you prefer to use a text logo -->
            <!--<h1><a href="#hero">Regna</a></h1>-->
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#hero">Home</a></li>
                <li><a href="#about">About Us</a></li>
<!--                <li class="menu-has-children"><a href="">Drop Down</a>-->
<!--                    <ul>-->
<!--                        <li><a href="#">Drop Down 1</a></li>-->
<!--                        <li class="menu-has-children"><a href="#">Drop Down 2</a>-->
<!--                            <ul>-->
<!--                                <li><a href="#">Deep Drop Down 1</a></li>-->
<!--                                <li><a href="#">Deep Drop Down 2</a></li>-->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!--                        <li><a href="#">Drop Down 3</a></li>-->
<!--                        <li><a href="#">Drop Down 4</a></li>-->
<!---->
<!--                    </ul>-->
<!--                </li>-->
                <li><a href="#contact">Contact Us</a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<!--==========================
  Hero Section
============================-->
<section id="hero">
    <div class="hero-container">
        <h1>Evaluate Students Engagement And Understanding Statuses In Classroom</h1>

        <a href="live_video.php" class="btn-get-started">Live</a>
        <a href="video_upload.html" class="btn-get-started">Video</a>
        <a href="imgUpload.php" class="btn-get-started">Picture</a>


        <!--<div class="pull-center">
          <div class="input-group">
              <span class="btn-get-started">
                  <span class="btn btn-default btn-file">
                      <input type="File" id="imgInp">
                  </span>
              </span>

          </div>
             <img id='img-upload'/>
         </div>-->
    </div>


</section><!-- #hero -->

<main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container">
            <div class="row about-container">

                <div class="col-lg-6 content order-lg-1 order-2">
                    <h2 class="title">Few Words About Us</h2>
                    <p>
                        We a group of undergrad students from united international university having five qualified members in team.  We started our thesis on emotion detection on summer 2019. In the final year design project we decided our work should be on machine learning. We built up this team where we worked our best to built up this system.
                    </p>


                    <div class="icon-box wow fadeInUp">
                        <div class="icon"><i class="fa fa-shopping-bag"></i></div>
                        <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
                        <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
                    </div>

                    <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
                        <div class="icon"><i class="fa fa-photo"></i></div>
                        <h4 class="title"><a href="">Magni Dolores</a></h4>
                        <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                    </div>

                    <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
                        <div class="icon"><i class="fa fa-bar-chart"></i></div>
                        <h4 class="title"><a href="">Dolor Sitema</a></h4>
                        <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                    </div>

                </div>

                <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight"></div>
            </div>

        </div>
    </section><!-- #about -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
        <div class="container wow fadeInUp">
            <div class="section-header">
                <h3 class="section-title">Contact</h3>
                <p class="section-description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </div>
        </div>

        <div class="container wow fadeInUp mt-5">
            <div class="row justify-content-center">

                <div class="col-lg-3 col-md-4">

                    <div class="info">
                        <div>
                            <i class="fa fa-map-marker"></i>
                            <p>United International University<br>Madani Avenue, Dhaka-1212</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p>mshakib151328@bscse.uiu.ac.bd</p>
                        </div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>+880-1854386898</p>
                        </div>
                    </div>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                </div>

                <div class="col-lg-5 col-md-8">
                    <div class="form">
                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage"></div>
                        <form action="" method="post" role="form" class="contactForm">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                <div class="validation"></div>
                            </div>
                            <div class="text-center"><button type="submit">Send Message</button></div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- #contact -->

</main>

<?php include_once ('footer.php')?>

<?php
include_once('header.php');
include ('DataProcessing.php');
session_start();

$object = new DataProcessing();

$startID = $_SESSION['startDateTime'];
$endID = $_SESSION['endDateTime'];

$studentsName = $object->studentsName($startID, $endID);
$studentsExpression = $object->studentsExpression($startID, $endID);
//var_dump($studentsExpression[4]['name']);
//var_dump($studentsName);
?>


    <!--==========================
      Header
      ============================-->
    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left-up">
                <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
                <!-- Uncomment below if you prefer to use a text logo -->
                <!--<h1><a href="#hero">Regna</a></h1>-->
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="index.php">Home</a></li>
                  
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->


    <!--==========================
     Hero Section
   ============================-->


    <section id="hero">
        <div class="hero-container">
            <div class="container">
                <div class="row" style="padding-left: 35px">
                    <h2>Total Students' Expression Statuses</h2>
                </div>
                <div class="row table-wrapper-scroll-y my-custom-scrollbar custom">
                    <table class="table table-hover table-dark">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Neutral(%)</th>
                            <th scope="col">Surprised(%)</th>
                            <th scope="col">Happy(%)</th>
                            <th scope="col">Angry(%)</th>
                            <th scope="col">Disgusted(%)</th>
                            <th scope="col">Fearful(%)</th>
                            <th scope="col">Sad(%)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for ($x=0; $x<sizeof($studentsName); $x++):?>
                            <?php
                            $neutral = 0;
                            $surprised = 0;
                            $happy = 0;
                            $angry = 0;
                            $disgusted = 0;
                            $fearful = 0;
                            $sad = 0;
                            for ($i=0; $i<sizeof($studentsExpression); $i++){
                                if ($studentsName[$x]['name'] == $studentsExpression[$i]['name']){
                                    if ($studentsExpression[$i]['expression'] == 'Happy'){
                                        $happy++;
                                    }
                                    elseif ($studentsExpression[$i]['expression'] == 'Neutral'){
                                        $neutral++;
                                    }
                                    elseif ($studentsExpression[$i]['expression'] == 'Surprised'){
                                        $surprised++;
                                    }
                                    elseif ($studentsExpression[$i]['expression'] == 'Sad'){
                                        $sad++;
                                    }
                                    elseif ($studentsExpression[$i]['expression'] == 'Fearful'){
                                        $fearful++;
                                    }
                                    elseif ($studentsExpression[$i]['expression'] == 'Disgusted'){
                                        $disgusted++;
                                    }
                                    elseif ($studentsExpression[$i]['expression'] == 'Angry'){
                                        $angry++;
                                    }
                                }
                                $totalExpCount = $happy+$neutral+$surprised+$sad+$fearful+$disgusted+$angry;

                            }
                            //                            echo $activePercentage.'%';
                            ?>
                            <tr>
                                <td><?php echo $x+1;?></td>
                                <td>
                                    <?php echo $studentsName[$x]['name'];?>
                                </td>
                                <td>
                                    <?php echo round($neutral/$totalExpCount*100).'%'?>
                                </td>
                                <td>
                                    <?php echo round($surprised/$totalExpCount*100).'%'?>
                                </td>
                                <td>
                                    <?php echo round($happy/$totalExpCount*100).'%'?>
                                </td>
                                <td>
                                    <?php echo round($angry/$totalExpCount*100).'%'?>
                                </td>
                                <td>
                                    <?php echo round($disgusted/$totalExpCount*100).'%'?>
                                </td>
                                <td>
                                    <?php echo round($fearful/$totalExpCount*100).'%'?>
                                </td>
                                <td>
                                    <?php echo round($sad/$totalExpCount*100).'%'?>
                                </td>
                            </tr>
                        <?php endfor;?>
                        </tbody>
                    </table>
                </div>
                <div class="row" style="margin-left: 300px">
                    <a href="live_video.php"><button type="button" class="btn btn-info">Test Again</button></a>
                    <div class="pull-right">
                        <a href="individualstatus.php"><button type="button" class="btn btn-info">Back</button></a>

                        <button onclick="myFunction()" class="btn btn-info">Print this page</button>
                        <script>
                            function myFunction() {
                                window.print();
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- #hero -->

<?php include_once ('footer.php');?>













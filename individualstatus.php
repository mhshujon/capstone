<?php
include_once('header.php');
include ('DataProcessing.php');
session_start();

$object = new DataProcessing();

$startID = $_SESSION['startDateTime'];
$endID = $_SESSION['endDateTime'];

$studentsName = $object->studentsName($startID, $endID);
$studentsExpression = $object->studentsExpression($startID, $endID);
//var_dump($studentsExpression[4]['name']);
//var_dump($studentsName);
?>


    <!--==========================
      Header
      ============================-->
<head>

  <title>Individual Status</title>
  
  
</head>

    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left-up">
                <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
                <!-- Uncomment below if you prefer to use a text logo -->
                <!--<h1><a href="#hero">Regna</a></h1>-->
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="index.php">Home</a></li>
                  
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->


    <!--==========================
     Hero Section
   ============================-->




    <section id="hero">
        <div class="hero-container">
            <div class="container" style="margin-top: 100px">
                <div class="row" style="padding-left: 35px">
                    <h2>Students' Individual Activity Statuses</h2>
                </div>
                <div class="row">
                    <table class="table table-hover table-dark">
                        <thead class="">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Active Status(%)</th>
                            <th scope="col">Inactive Status(%)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for ($x=0; $x<sizeof($studentsName); $x++):?>
                            <tr>
                                <td><?php echo $x+1;?></td>
                                <td>
                                    <?php
                                    echo $studentsName[$x]['name'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $active = 0;
                                    $inactive = 0;
                                    for ($i=0; $i<sizeof($studentsExpression); $i++){
                                        if ($studentsName[$x]['name'] == $studentsExpression[$i]['name']){
                                            if ($studentsExpression[$i]['expression'] == 'Happy' || $studentsExpression[$i]['expression'] == 'Surprised' || $studentsExpression[$i]['expression'] == 'Neutral'){
                                                $active++;
                                            }
                                            else{
                                                $inactive++;
                                            }
//                                            echo $studentsExpression[$i]['expression'];
                                        }
                                    }
//                                    echo $active.'|';
//                                    echo $inactive;
                                    $activePercentage = ($active/($active+$inactive)) * 100;
                                    echo round($activePercentage).'%';
//                                    echo $studentsName[$x]['name'];
//                                    if ($studentsName[$x]['name'] == $studentsExpression[$i]['name']){
//                                        echo $studentsExpression[$i]['name'];
//                                    }
//
//                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $inactivePercentage = ($inactive/($active+$inactive)) * 100;
                                    echo round($inactivePercentage).'%';
                                    ?>
                                </td>
                            </tr>
                        <?php endfor;?>
                        </tbody>
                        </tbody>
                        </tbody>

                    </table>
                </div>
                <div class="row" style="margin-left: 300px">
                    <a href="live_video.php"><button type="button" class="btn btn-info">Test Again</button></a>
                    <div class="pull-right">
                        <a href="individualexpressioncount.php"><button type="button" class="btn btn-info">Individual Expression</button></a>
                        <a href="totalstatus.php"><button type="button" class="btn btn-info">Back</button></a>

                        <button onclick="myFunction()" class="btn btn-info">Print this page</button>
                        <script>
                            function myFunction() {
                                window.print();
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- #hero -->

<?php include_once ('footer.php');?>








<!--<div class="row">-->
<!--    <div class="row">-->
<!--        <h1>Students' Statuses</h1>-->
<!--    </div>-->
<!--    <div class="row container table-wrapper-scroll-y my-custom-scrollbar custom">-->
<!--        <div class="row">-->
<!--            <table class="table table-hover table-dark">-->
<!--                <thead>-->
<!--                <tr>-->
<!--                    <th scope="col">#</th>-->
<!--                    <th scope="col">Student's Name</th>-->
<!--                    <th scope="col">Active Status (%)</th>-->
<!--                    <th scope="col">Inctive Status (%)</th>-->
<!--                </tr>-->
<!--                </thead>-->
<!--                <tbody>-->
<!--                --><?php //for ($i=0; $i<sizeof($data); $i++):?>
<!--                    <tr>-->
<!--                        <td scope="row">--><?php //echo $i+1?><!--</td>-->
<!--                        <td>--><?php //echo $data[$i]['name']?><!--</td>-->
<!--                        <td>-->
<!--                            --><?php
//                            if ((float)$data[$i]['angry'] <= 1 && (float)$data[$i]['angry'] >= 0.7){
//                                echo 'Angry';
//                                $expression = 'angry';
//                            }
//                            elseif ((float)$data[$i]['disgusted'] <= 1 && (float)$data[$i]['disgusted'] >= 0.7){
//                                echo 'Disgusted';
//                                $expression = 'disgusted';
//                            }
//                            elseif ((float)$data[$i]['fearful'] <= 1 && (float)$data[$i]['fearful'] >= 0.7){
//                                echo 'Fearful';
//                                $expression = 'fearful';
//                            }
//                            elseif ((float)$data[$i]['happy'] <= 1 && (float)$data[$i]['happy'] >= 0.7){
//                                echo 'Happy';
//                                $expression = 'happy';
//                            }
//                            elseif ((float)$data[$i]['neutral'] <= 1 && (float)$data[$i]['neutral'] >= 0.7){
//                                echo 'Neutral';
//                                $expression = 'neutral';
//                            }
//                            elseif ((float)$data[$i]['sad'] <= 1 && (float)$data[$i]['sad'] >= 0.7){
//                                echo 'Sad';
//                                $expression = 'sad';
//                            }
//                            elseif ((float)$data[$i]['surprised'] <= 1 && (float)$data[$i]['surprised'] >= 0.7){
//                                echo 'Surprised';
//                                $expression = 'surprised';
//                            }
//                            ?>
<!--                        </td>-->
<!--                        <td style="color: --><?php
//                        if ($expression == 'angry' || $expression == 'disgusted' || $expression == 'fearful' || $expression == 'sad' || $expression == 'surprised')
//                            echo 'crimson';
//                        else
//                            echo 'green';
//                        ?><!--">-->
<!--                            --><?php
//                            if ($expression == 'angry'){
//                                echo 'Needs extra attention!';
//                            }
//                            elseif ($expression == 'disgusted'){
//                                echo 'Needs extra attention!';
//                            }
//                            elseif ($expression == 'fearful'){
//                                echo 'Needs extra attention!';
//                            }
//                            elseif ($expression == 'happy'){
//                                echo 'Seems okay!';
//                            }
//                            elseif ($expression == 'neutral'){
//                                echo 'Seems okay!';
//                            }
//                            elseif ($expression == 'sad'){
//                                echo 'Needs extra attention!';
//                            }
//                            elseif ($expression == 'surprised'){
//                                echo 'Seems okay!';
//                            }
//                            ?>
<!--                        </td>-->
<!--                    </tr>-->
<!--                --><?php //endfor;?>
<!--                </tbody>-->
<!---->
<!--            </table>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="row">-->
<!--        <a href="live_video.php"><button type="button" class="btn btn-info">Test Again</button></a>-->
<!--        <div class="pull-right">-->
<!--            <a href="assist-view.php"><button type="button" class="btn btn-info">Back</button></a>-->
<!---->
<!--            <button onclick="myFunction()" class="btn btn-info">Print this page</button>-->
<!--            <script>-->
<!--                function myFunction() {-->
<!--                    window.print();-->
<!--                }-->
<!--            </script>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--<div class="row">-->
<!--    <div class="col-md-6">-->
<!--        <div class="row">-->
<!--            <h1>Students' Statuses</h1>-->
<!--        </div>-->
<!--        <div class="row container table-wrapper-scroll-y my-custom-scrollbar custom">-->
<!--            <div class="row">-->
<!--                <table class="table table-hover table-dark">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th scope="col">#</th>-->
<!--                        <th scope="col">Student's Name</th>-->
<!--                        <th scope="col">Active Status (%)</th>-->
<!--                        <th scope="col">Inctive Status (%)</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    --><?php //for ($i=0; $i<sizeof($data); $i++):?>
<!--                        <tr>-->
<!--                            <td scope="row">--><?php //echo $i+1?><!--</td>-->
<!--                            <td>--><?php //echo $data[$i]['name']?><!--</td>-->
<!--                            <td>-->
<!--                                --><?php
//                                if ((float)$data[$i]['angry'] <= 1 && (float)$data[$i]['angry'] >= 0.7){
//                                    echo 'Angry';
//                                    $expression = 'angry';
//                                }
//                                elseif ((float)$data[$i]['disgusted'] <= 1 && (float)$data[$i]['disgusted'] >= 0.7){
//                                    echo 'Disgusted';
//                                    $expression = 'disgusted';
//                                }
//                                elseif ((float)$data[$i]['fearful'] <= 1 && (float)$data[$i]['fearful'] >= 0.7){
//                                    echo 'Fearful';
//                                    $expression = 'fearful';
//                                }
//                                elseif ((float)$data[$i]['happy'] <= 1 && (float)$data[$i]['happy'] >= 0.7){
//                                    echo 'Happy';
//                                    $expression = 'happy';
//                                }
//                                elseif ((float)$data[$i]['neutral'] <= 1 && (float)$data[$i]['neutral'] >= 0.7){
//                                    echo 'Neutral';
//                                    $expression = 'neutral';
//                                }
//                                elseif ((float)$data[$i]['sad'] <= 1 && (float)$data[$i]['sad'] >= 0.7){
//                                    echo 'Sad';
//                                    $expression = 'sad';
//                                }
//                                elseif ((float)$data[$i]['surprised'] <= 1 && (float)$data[$i]['surprised'] >= 0.7){
//                                    echo 'Surprised';
//                                    $expression = 'surprised';
//                                }
//                                ?>
<!--                            </td>-->
<!--                            <td style="color: --><?php
//                            if ($expression == 'angry' || $expression == 'disgusted' || $expression == 'fearful' || $expression == 'sad' || $expression == 'surprised')
//                                echo 'crimson';
//                            else
//                                echo 'green';
//                            ?><!--">-->
<!--                                --><?php
//                                if ($expression == 'angry'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'disgusted'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'fearful'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'happy'){
//                                    echo 'Seems okay!';
//                                }
//                                elseif ($expression == 'neutral'){
//                                    echo 'Seems okay!';
//                                }
//                                elseif ($expression == 'sad'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'surprised'){
//                                    echo 'Seems okay!';
//                                }
//                                ?>
<!--                            </td>-->
<!--                        </tr>-->
<!--                    --><?php //endfor;?>
<!--                    </tbody>-->
<!---->
<!--                </table>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <a href="live_video.php"><button type="button" class="btn btn-info">Test Again</button></a>-->
<!--            <div class="pull-right">-->
<!--                <a href="assist-view.php"><button type="button" class="btn btn-info">Back</button></a>-->
<!---->
<!--                <button onclick="myFunction()" class="btn btn-info">Print this page</button>-->
<!--                <script>-->
<!--                    function myFunction() {-->
<!--                        window.print();-->
<!--                    }-->
<!--                </script>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="col-md-6">-->
<!--        <div class="row">-->
<!--            <h1>Students' Statuses</h1>-->
<!--        </div>-->
<!--        <div class="row container table-wrapper-scroll-y my-custom-scrollbar custom">-->
<!--            <div class="row">-->
<!--                <table class="table table-hover table-dark">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th scope="col">#</th>-->
<!--                        <th scope="col">Student's Name</th>-->
<!--                        <th scope="col">Active Status (%)</th>-->
<!--                        <th scope="col">Inctive Status (%)</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    --><?php //for ($i=0; $i<sizeof($data); $i++):?>
<!--                        <tr>-->
<!--                            <td scope="row">--><?php //echo $i+1?><!--</td>-->
<!--                            <td>--><?php //echo $data[$i]['name']?><!--</td>-->
<!--                            <td>-->
<!--                                --><?php
//                                if ((float)$data[$i]['angry'] <= 1 && (float)$data[$i]['angry'] >= 0.7){
//                                    echo 'Angry';
//                                    $expression = 'angry';
//                                }
//                                elseif ((float)$data[$i]['disgusted'] <= 1 && (float)$data[$i]['disgusted'] >= 0.7){
//                                    echo 'Disgusted';
//                                    $expression = 'disgusted';
//                                }
//                                elseif ((float)$data[$i]['fearful'] <= 1 && (float)$data[$i]['fearful'] >= 0.7){
//                                    echo 'Fearful';
//                                    $expression = 'fearful';
//                                }
//                                elseif ((float)$data[$i]['happy'] <= 1 && (float)$data[$i]['happy'] >= 0.7){
//                                    echo 'Happy';
//                                    $expression = 'happy';
//                                }
//                                elseif ((float)$data[$i]['neutral'] <= 1 && (float)$data[$i]['neutral'] >= 0.7){
//                                    echo 'Neutral';
//                                    $expression = 'neutral';
//                                }
//                                elseif ((float)$data[$i]['sad'] <= 1 && (float)$data[$i]['sad'] >= 0.7){
//                                    echo 'Sad';
//                                    $expression = 'sad';
//                                }
//                                elseif ((float)$data[$i]['surprised'] <= 1 && (float)$data[$i]['surprised'] >= 0.7){
//                                    echo 'Surprised';
//                                    $expression = 'surprised';
//                                }
//                                ?>
<!--                            </td>-->
<!--                            <td style="color: --><?php
//                            if ($expression == 'angry' || $expression == 'disgusted' || $expression == 'fearful' || $expression == 'sad' || $expression == 'surprised')
//                                echo 'crimson';
//                            else
//                                echo 'green';
//                            ?><!--">-->
<!--                                --><?php
//                                if ($expression == 'angry'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'disgusted'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'fearful'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'happy'){
//                                    echo 'Seems okay!';
//                                }
//                                elseif ($expression == 'neutral'){
//                                    echo 'Seems okay!';
//                                }
//                                elseif ($expression == 'sad'){
//                                    echo 'Needs extra attention!';
//                                }
//                                elseif ($expression == 'surprised'){
//                                    echo 'Seems okay!';
//                                }
//                                ?>
<!--                            </td>-->
<!--                        </tr>-->
<!--                    --><?php //endfor;?>
<!--                    </tbody>-->
<!---->
<!--                </table>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <a href="live_video.php"><button type="button" class="btn btn-info">Test Again</button></a>-->
<!--            <div class="pull-right">-->
<!--                <a href="assist-view.php"><button type="button" class="btn btn-info">Back</button></a>-->
<!---->
<!--                <button onclick="myFunction()" class="btn btn-info">Print this page</button>-->
<!--                <script>-->
<!--                    function myFunction() {-->
<!--                        window.print();-->
<!--                    }-->
<!--                </script>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->




<?php include_once ('header.php');
session_start();
session_destroy();
?>

    <!--==========================
      Header
      ============================-->
    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left-up">
                <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
                <!-- Uncomment below if you prefer to use a text logo -->
                <!--<h1><a href="#hero">Regna</a></h1>-->
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="index.php">Home</a></li>
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#contact">Contact Us</a></li>
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->


    <!--==========================
     Hero Section
   ============================-->
    <section id="hero">
        <div class="hero-container">
            <h1>Live</h1>
            <video id="video" width="720" height="560" autoplay muted></video>

            <div style="color: greenyellow">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Name:</h5>
                    </div>
                    <div class="col-md-6">
                        <h5 id="name"></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h5>Expression:</h5>
                    </div>
                    <div class="col-md-6">
                        <h5 id="expression"></h5>
                    </div>
                </div>
            </div>

<!--            <a href="assist-view.php" class="btn-get-started">Show Result</a>-->
<!--            <a href=""><button type="button" class="btn btn-info">Show Result</button></a>-->
            <div class="row">
                <a href="assist-view.php"><button type="button" class="btn btn-info">Result View</button></a>
            </div>

        </div>

    </section><!-- #hero -->

<?php include_once ('footer.php');?>
const imageUpload = document.getElementById('imageUpload') //assigning the uploaded image into imageUpload

//adding models
Promise.all([
  faceapi.nets.faceRecognitionNet.loadFromUri('/capstone/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/capstone/models'),
  faceapi.nets.ssdMobilenetv1.loadFromUri('/capstone/models'),
  faceapi.nets.tinyFaceDetector.loadFromUri('/capstone/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/capstone/models')
]).then(start)

//function start 
async function start() {
  const container = document.createElement('div')
  container.style.position = 'relative'
  document.body.append(container)
  const labeledFaceDescriptors = await loadLabeledImages() //it'll call the image with label those are in folder
  const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6) //load the face to match
  let image
  let canvas
  let name
  let expression
  let angry
  let disgusted
  let fearful
  let happy
  let neutral
  let sad
  let surprised
  document.body.append('Loaded')
  imageUpload.addEventListener('change', async () => {
    if (image) image.remove()
    if (canvas) canvas.remove()
    image = await faceapi.bufcapstoneToImage(imageUpload.files[0])
    container.append(image)
    canvas = faceapi.createCanvasFromMedia(image)
    container.append(canvas)
    const displaySize = { width: image.width, height: image.height }
    faceapi.matchDimensions(canvas, displaySize)
    const detections = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
    results.forEach((result, i) => {
      name = result.toString()
      console.log(result.toString()) //showing result in console. showing the name
      const box = resizedDetections[i].detection.box
      const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
      drawBox.draw(canvas)
    })
    //container.append(image)
    container.append(canvas)
    const detections2 = await faceapi.detectAllFaces(image, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
    const resizedDetections2 = faceapi.resizeResults(detections2, displaySize)
    //console.log(resizedDetections2[0].expressions)
    resizedDetections2.forEach((result2,i) => {
          //console.log(resizedDetections2[i])
          //console.log(result2.expressions) //showing the expressions result in console
          expression = result2.expressions
        }
    )
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections2)
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections2)
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections2)
    //const results2 = resizedDetections2.map(d => faceMatcher.findBestMatch(d.descriptor))
    //expression = resizedDetections2[0].expressions
    //insert(name,expression)
    console.log(expression)
    angry = expression.angry
    disgusted = expression.disgusted
    fearful = expression.fearful
    happy = expression.happy
    neutral = expression.neutral
    sad = expression.sad
    surprised = expression.surprised

    insert(name,angry,disgusted,fearful,happy,neutral,sad,surprised)
  })
}

function loadLabeledImages() {
  const labels = ['Rabbi','Ruhul', 'Monir'] //add new lebeled folder here
  return Promise.all(
      labels.map(async label => {
        const descriptions = []
        for (let i = 1; i <= 2; i++) {
          //fetching data from online. you can replace this with local directory
          const img = await faceapi.fetchImage(`/capstone/labeled_images/${label}/${i}.jpg`)
          const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
          descriptions.push(detections.descriptor)
        }

        return new faceapi.LabeledFaceDescriptors(label, descriptions)
      })
  )
}
//api call to pass data to php page
//function insert(name,expression) {
function insert(name,angry,disgusted,fearful,happy,neutral,sad,surprised) {
  $.ajax({
    type: 'POST',
    url: 'insert.php',
    data: {
      name:name,
      angry:angry,
      disgusted:disgusted,
      fearful:fearful,
      happy:happy,
      neutral:neutral,
      sad:sad,
      surprised:surprised
    },
    error: function (xhr, status) {
      // alert(status);
    },
    success: function(response) {
      alert(response);
      //alert("Status Accepted");
      //alert(response);
      // location.reload('view.php');
      window.location.href = "view.php"
    },

  });
}
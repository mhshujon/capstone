// const video = document.getElementById('video')
// let result
// let name
// let expression
// let angry
// let disgusted
// let fearful
// let happy
// let neutral
// let sad
// let surprised
//
// Promise.all([
//   faceapi.nets.faceRecognitionNet.loadFromUri('/capstone/models'),
//   faceapi.nets.faceLandmark68Net.loadFromUri('/capstone/models'),
//   faceapi.nets.ssdMobilenetv1.loadFromUri('/capstone/models'),
//   faceapi.nets.tinyFaceDetector.loadFromUri('/capstone/models'),
//   faceapi.nets.faceExpressionNet.loadFromUri('/capstone/models')
// ]).then(startVideo)
//
// function startVideo() {
//   navigator.getUserMedia(
//       { video: {} },
//       stream => video.srcObject = stream,
//       err => console.error(err)
//   )
// }
// video.addEventListener('play', () => {
//   const canvas = faceapi.createCanvasFromMedia(video)
//   document.body.append(canvas)
//   const displaySize = { width: video.width, height: video.height }
//   faceapi.matchDimensions(canvas, displaySize)
//   //const labeledFaceDescriptors = loadLabeledImages()
//   setInterval(async () => {
//     const labeledFaceDescriptors = await loadLabeledImages()
//     const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
//     const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions().withFaceDescriptors()
//     const detectionsWithExpressions = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions().withFaceDescriptors()
//     const resizedDetections = faceapi.resizeResults(detectionsWithExpressions, displaySize)
//     console.log(resizedDetections)
//     canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
//     faceapi.draw.drawDetections(canvas, resizedDetections)
//     faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
//     faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
//     const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
//
//
//
//
//
//
//     let flag1 = 0
//     let flag2 = 0
//
//     try {
//       resizedDetections[0].expressions
//       flag1=1
//     }
//     catch{
//       //console.log("Expression not found")
//     }
//     if (flag1==1) {
//       expression = resizedDetections[0].expressions
//       console.log(resizedDetections)
//       angry = expression.angry
//       disgusted = expression.disgusted
//       fearful = expression.fearful
//       happy = expression.happy
//       neutral = expression.neutral
//       sad = expression.sad
//       surprised = expression.surprised
//
//
//       if(angry > 0.90){
//         express = "Angry"
//       } else if(disgusted > 0.90){
//         express ="Disgusted"
//       } else if(fearful > 0.90){
//         express = "Fearful"
//       } else if(happy > 0.90){
//         express = "Happy"
//       } else if(neutral > 0.90){
//         express = "Neutral"
//       } else if(sad > 0.90){
//         express = "Sad"
//       } else if(surprised > 0.90){
//         express = "Surprised"
//       }
//       else{
//         express = "Unknown"
//       }
//       console.log(express)
//     }
//     try {
//       results[0].toString()
//       flag2=1
//     } catch {
//       // console.log("Face not found")
//     }
//     if (flag2==1) {
//       name = results[0].toString()
//       console.log(results[0].toString())
//     }
//     if(name.includes("unknown")){
//       insertUnknownFace(resizedDetections)
//       console("Inseting unknown face")
//     }
//     else if (flag1 == 1 && flag2 == 1){
//       insert(name,express,angry,disgusted,fearful,happy,neutral,sad,surprised)
//       document.getElementById('name').innerHTML = name
//       document.getElementById('expression').innerHTML = express
//     }
//
//
//
//   }, 500)
// })
//
// function loadLabeledImages() {
//   const labels = ['Rabbi'] //add new lebeled folder here
//   return Promise.all(
//       labels.map(async label => {
//         const descriptions = []
//         for (let i = 1; i <= 2; i++) {
//           //fetching data from online. you can replace this with local directory
//           const img = await faceapi.fetchImage(`/capstone/labeled_images/${label}/${i}.jpg`)
//           //console.log(img)
//           const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
//           //console.log(detections);
//           descriptions.push(detections.descriptor)
//           // console.log("Training...")
//         }
//
//         return new faceapi.LabeledFaceDescriptors(label, descriptions)
//       })
//   )
// }
//
// function insert(name,express,angry,disgusted,fearful,happy,neutral,sad,surprised) {
//   $.ajax({
//     type: 'POST',
//     url: 'insert.php',
//     data: {
//       name:name,
//       express:express,
//       angry:angry,
//       disgusted:disgusted,
//       fearful:fearful,
//       happy:happy,
//       neutral:neutral,
//       sad:sad,
//       surprised:surprised
//     },
//     error: function (xhr, status) {
//       // alert(status);
//     },
//     success: function(response) {
//       // alert(response);
//       //alert("Status Accepted");
//       //alert(response);
//       //location.reload();
//     }
//   });
// }
// function insertUnknownFace(unknownFaceObject) {
//   $.ajax({
//     type: 'POST',
//     url: 'insertUnknownFace.php',
//     data: {
//       unknownFaceObject:unknownFaceObject
//     },
//     error: function (xhr, status) {
//       // alert(status);
//     },
//     success: function(response) {
//       alert(response);
//       //alert("Status Accepted");
//       //alert(response);
//       //location.reload();
//     }
//   });
// }

const video = document.getElementById('video')
let result
let name
let expression
let angry
let disgusted
let fearful
let happy
let neutral
let sad
let surprised

Promise.all([
  faceapi.nets.faceRecognitionNet.loadFromUri('/capstone/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/capstone/models'),
  faceapi.nets.ssdMobilenetv1.loadFromUri('/capstone/models'),
  faceapi.nets.tinyFaceDetector.loadFromUri('/capstone/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/capstone/models')
]).then(startVideo)

function startVideo() {
  navigator.getUserMedia(
      { video: {} },
      stream => video.srcObject = stream,
      err => console.error(err)
  )
}
video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  //const labeledFaceDescriptors = loadLabeledImages()
  setInterval(async () => {
    const labeledFaceDescriptors = await loadLabeledImages()
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions().withFaceDescriptors()
    const detectionsWithExpressions = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detectionsWithExpressions, displaySize)
    // console.log(resizedDetections)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
    const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))






    let flag1 = 0
    let flag2 = 0

    try {
      resizedDetections[0].expressions
      flag1=1
    }
    catch{
      //console.log("Expression not found")
    }
    if (flag1==1) {
      expression = resizedDetections[0].expressions
      // console.log(resizedDetections)
      angry = expression.angry
      disgusted = expression.disgusted
      fearful = expression.fearful
      happy = expression.happy
      neutral = expression.neutral
      sad = expression.sad
      surprised = expression.surprised


      if(angry > 0.90){
        express = "Angry"
      } else if(disgusted > 0.90){
        express ="Disgusted"
      } else if(fearful > 0.90){
        express = "Fearful"
      } else if(happy > 0.90){
        express = "Happy"
      } else if(neutral > 0.90){
        express = "Neutral"
      } else if(sad > 0.90){
        express = "Sad"
      } else if(surprised > 0.90){
        express = "Surprised"
      }
      else{
        express = "Unknown"
      }
      console.log(express)
    }
    try {
      results[0].toString()
      flag2=1
    } catch {
      // console.log("Face not found")
    }
    if (flag2==1) {
      name = results[0].toString()
      console.log(results[0].toString())
    }
    if(name.includes("unknown")){
      uface(resizedDetections)
      // console.log("Inseting unknown face")
      console.log(resizedDetections)
    }
    else if (flag1 == 1 && flag2 == 1){
      insert(name,express,angry,disgusted,fearful,happy,neutral,sad,surprised)
      document.getElementById('name').innerHTML = name
      document.getElementById('expression').innerHTML = express
    }



  }, 500)
})

function loadLabeledImages() {
  const labels = ['Rabbi'] //add new lebeled folder here
  return Promise.all(
      labels.map(async label => {
        const descriptions = []
        for (let i = 1; i <= 2; i++) {
          //fetching data from online. you can replace this with local directory
          const img = await faceapi.fetchImage(`/capstone/labeled_images/${label}/${i}.jpg`)
          //console.log(img)
          const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
          //console.log(detections);
          descriptions.push(detections.descriptor)
          // console.log("Training...")
        }

        return new faceapi.LabeledFaceDescriptors(label, descriptions)
      })
  )
}

function insert(name,express,angry,disgusted,fearful,happy,neutral,sad,surprised) {
  $.ajax({
    type: 'POST',
    url: 'insert.php',
    data: {
      name:name,
      express:express,
      angry:angry,
      disgusted:disgusted,
      fearful:fearful,
      happy:happy,
      neutral:neutral,
      sad:sad,
      surprised:surprised
    },
    error: function (xhr, status) {
      // alert(status);
    },
    success: function(response) {
      // alert(response);
      //alert("Status Accepted");
      //alert(response);
      //location.reload();
    }
  });
}
function uface(resizedDetections) {
  $.ajax({
    type: 'POST',
    url: 'insertUnknownFace.php',
    data: {
      resizedDetections:resizedDetections
    },
    error: function (xhr, status) {
      // alert(status);
    },
    success: function(response) {
      // alert(response);
      //alert("Status Accepted");
      //alert(response);
      //location.reload();
      // console.log(response)
    }
  });
}
<?php
include_once('header.php');
include ('DataProcessing.php');
session_start();

$object = new DataProcessing();

if (isset($_POST['startDateTime']) && isset($_POST['endDateTime'])){
    $_SESSION['startDateTime'] = $_POST['startDateTime'];
    $_SESSION['endDateTime'] = $_POST['endDateTime'];
}
$startID = $_SESSION['startDateTime'];
$endID = $_SESSION['endDateTime'];

$totalStudents = $object->totalStudents($startID, $endID);
$studentsExpression = $object->studentsExpression($startID, $endID);
//var_dump($studentsExpression[0]['expression']);
//echo sizeof($studentsExpression);
?>


    <!--==========================
      Header
      ============================-->
<head>

  <title>Total Status</title>
  
  
</head>

    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left-up">
                <a href="#hero"><img src="img/rsz_logo.png" alt="uiu" title="" /></img></a>
                <!-- Uncomment below if you prefer to use a text logo -->
                <!--<h1><a href="#hero">Regna</a></h1>-->
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="index.php">Home</a></li>
                  
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->

    <!--==========================
     Hero Section
   ============================-->




<section id="hero">
    <div class="hero-container">
        <div class="container">
            <div class="row" style="padding-left: 35px">
                <h2>Total Students' Activity Statuses</h2>
            </div>
            <div class="row">
                <table class="table table-hover table-dark">
                    <thead class="">
                    <tr>
                        <th scope="col">Total Students</th>
                        <th scope="col">Active Status (%)</th>
                        <th scope="col">Inactive Status (%)</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $active = 0;
                            $inactive = 0;
                            for ($i=0; $i<sizeof($studentsExpression); $i++){
                                if ($studentsExpression[$i]['expression'] == 'Happy' || $studentsExpression[$i]['expression'] == 'Surprised' || $studentsExpression[$i]['expression'] == 'Neutral'){
                                    $active++;
                                }
                                else{
                                    $inactive++;
                                }
                            }
                            $activePercentage = ($active/($active+$inactive)) * 100;
                            $inactivePercentage = ($inactive/($active+$inactive)) * 100;
                            ?>
                            <td><?php echo $totalStudents[0]['totalStudents']?></td>
                            <td><?php echo round($activePercentage).'%';?></td>
                            <td><?php echo round($inactivePercentage).'%';?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row" style="margin-left: 300px">
                <a href="live_video.php"><button type="button" class="btn btn-info">Test Again</button></a>
                <div class="pull-right">
                    <a href="individualstatus.php"><button type="button" class="btn btn-info">Individual Status</button></a>
                    <a href="assist-view.php"><button type="button" class="btn btn-info">Back</button></a>

                    <button onclick="myFunction()" class="btn btn-info">Print this page</button>
                    <script>
                        function myFunction() {
                            window.print();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>

</section><!-- #hero -->

<?php include_once ('footer.php');?>












